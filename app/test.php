<?php

namespace App;

use Terranet\Administrator\Contracts\Module\Navigable;

class Test implements Navigable
{
    /**
     * Navigation container which Resource belongs to
     * Available: sidebar, tools.
     *
     * @return mixed
     */
    public function navigableIn(){
        return Navigable::MENU_TOOLS;
    }

    /**
     * Add resource to navigation if condition accepts.
     *
     * @return mixed
     */
    public function showIf(){
        return true;
    }

    /**
     * Add resource to navigation as link or header.
     *
     * @return mixed
     */
    public function showAs(){
        return true;
    }

    /**
     * Navigation group which Resource belongs to.
     *
     * @return string
     */
    public function group(){
        return true;
    }

    /**
     * Resource order number.
     *
     * @return int
     */
    public function order(){
        return true;
    }

    /**
     * Attributes assigned to <a> element.
     *
     * @return mixed
     */
    public function linkAttributes(){
        return true;
    }
    public function url()
{
    return "articles";
}
}
