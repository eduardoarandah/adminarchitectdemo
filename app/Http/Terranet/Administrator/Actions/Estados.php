<?php

namespace App\Http\Terranet\Administrator\Actions;

use Terranet\Administrator\Services\CrudActions;

class Estados extends CrudActions
{
    public function actions()
    {
        return [
            // CustomAction::class
        ];
    }

    public function batchActions()
    {
        return array_merge(parent::batchActions(), [
            // CustomAction::class
        ]);
    }
    public function canDelete($user, $entity)
    {
        return false;
    }

    public function canUpdate($user, $entity)
    {
        return true;
    }

    public function canView($user, $entity)
    {
        return true;
    }
}