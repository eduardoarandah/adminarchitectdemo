<?php

namespace App\Http\Terranet\Administrator\Actions;

use Terranet\Administrator\Services\CrudActions;

class Municipios extends CrudActions
{
    public function actions()
    {
        return [
            // CustomAction::class
        ];
    }

    public function batchActions()
    {
        return array_merge(parent::batchActions(), [
            // CustomAction::class
        ]);
    }
    public function canDelete($user, $entity)
    {
        return false;
    }

    public function canUpdate($user, $entity)
    {
        return false;
    }

    public function canView($user, $entity)
    {
        return true;
    }
}