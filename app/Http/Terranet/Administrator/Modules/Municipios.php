<?php

namespace App\Http\Terranet\Administrator\Modules;

use Terranet\Administrator\Contracts\Module\Editable;
use Terranet\Administrator\Contracts\Module\Exportable;
use Terranet\Administrator\Contracts\Module\Filtrable;
use Terranet\Administrator\Contracts\Module\Navigable;
use Terranet\Administrator\Contracts\Module\Sortable;
use Terranet\Administrator\Contracts\Module\Validable;
use Terranet\Administrator\Filters\FilterElement;
use Terranet\Administrator\Scaffolding;
use Terranet\Administrator\Traits\Module\AllowFormats;
use Terranet\Administrator\Traits\Module\AllowsNavigation;
use Terranet\Administrator\Traits\Module\HasFilters;
use Terranet\Administrator\Traits\Module\HasForm;
use Terranet\Administrator\Traits\Module\HasSortable;
use Terranet\Administrator\Traits\Module\ValidatesForm;

/**
 * Administrator Resource Municipios
 *
 * @package Terranet\Administrator
 */
class Municipios extends Scaffolding implements Navigable, Filtrable, Editable, Validable, Sortable, Exportable
{
    use HasFilters, HasForm, HasSortable, ValidatesForm, AllowFormats, AllowsNavigation;

    /**
     * The module Eloquent model
     *
     * @var string
     */
    protected $model = 'App\\Models\\Municipio';
    protected $actions =null;

    public function filters()
    {

        return $this
        // # Preserve auto-discovered filters
        ->scaffoldFilters()
            ->push(FilterElement::text('nombre'))

        // # optionaly remove unnecessary
             // ->without(['actions'])

        // # let's filter our collection by user_id column
            ->push(
                FilterElement::select('estado_id', [], $this->estados())
            );

        // # optionaly for foreign columns we can define a custom query
        //     ->update('user_id', function ($userId) {
        //         $userId
        //             ->getInput()
        //             ->setQuery(function ($query, $value) {
        //                 return $query->whereIn('user_id', [$value]);
        //             });

        //         return $userId;
        //     });
    }
    // public function actions()
    // {
    // 	return [];
    // }
    public function estados()
    {
        return ['' => '--Cualquiera--']+\App\Models\Estado::pluck('nombre', 'id')->toArray();
    }

    public function columns()
    {
        return $this->scaffoldColumns()            
            ->without(['slug']);
    }

    public function sortable()
    {
        return [
            'id', 'estado_id', 'nombre', 'slug',
        ];
    }

    public function perPage()
    {
        return 50;
    }

    public function group()
    {
        return "Catálogos";
    }
}
