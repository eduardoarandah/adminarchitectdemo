<?php

namespace App\Models;

use App\Presenters\EstadoPresenter;
use Illuminate\Database\Eloquent\Model;

use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;


/**
 * App\Models\Estado
 *
 * @property int $id
 * @property string $nombre
 * @property string $abreviacion
 * @property string $slug
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Municipio[] $municipio
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Estado whereAbreviacion($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Estado whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Estado whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Estado whereSlug($value)
 * @mixin \Eloquent
 */
class Estado extends Model implements \Terranet\Presentable\PresentableInterface, StaplerableInterface
{
    use \Terranet\Presentable\PresentableTrait;
    use EloquentTrait;

    protected $presenter = EstadoPresenter::class;

    protected $table   = 'estado';
    public $timestamps = false;

    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'abreviacion', 'slug','foto'];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('foto', [
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);        
        parent::__construct($attributes);
    }

    public function municipio()
    {
        return $this->hasMany('App\Models\Municipio');
    }
}
