<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Municipio
 *
 * @property int $id
 * @property int $estado_id
 * @property string $nombre
 * @property string $slug
 * @property-read \App\Models\Estado $estado
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Municipio whereEstadoId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Municipio whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Municipio whereNombre($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Municipio whereSlug($value)
 * @mixin \Eloquent
 */
class Municipio extends Model
{

    public $timestamps = false;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'municipio';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['estado_id', 'nombre', 'slug'];

    /**
     * @editable 
     * @widget
     * 
     */
    public function estado()
    {
        return $this->belongsTo('App\Models\Estado');
    }

    //scopes
    public function scopeEstado($query,$estado_id)
    {
        return $query->where('estado_id', $estado_id);
    }
    
}
