<?php

namespace App\Models;

use Codesleeve\Stapler\ORM\EloquentTrait;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements StaplerableInterface
{
    use Notifiable;

    //usar https://github.com/CodeSleeve/laravel-stapler para guardar imagenes
    use EloquentTrait;
    public function __construct(array $attributes = array())
    {
        $this->hasAttachedFile('avatar', [
            # Image resizing configuration
            'styles' => [
                'medium' => '300x300',
                'thumb' => '100x100'
            ]
        ]);
        parent::__construct($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];
}
