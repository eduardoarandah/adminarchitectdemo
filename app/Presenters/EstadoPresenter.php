<?php

namespace App\Presenters;

use Terranet\Presentable\Presenter;

class EstadoPresenter extends Presenter
{
    public function municipios()
    {
        return link_to_route('scaffold.view', "ver municipios", ['module' => 'municipios', null, 'estado_id' => $this->presentable->id]);
    }

}
