<?php

use Illuminate\Database\Seeder;
use App\Models\Estado;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $estados = [
            ['id'=>'01', 'nombre'=>'Aguascalientes', 'abreviacion'=>'Ags'],
            ['id'=>'02', 'nombre'=>'Baja California', 'abreviacion'=>'BC'],
            ['id'=>'03', 'nombre'=>'Baja California Sur', 'abreviacion'=>'BCS'],
            ['id'=>'04', 'nombre'=>'Campeche', 'abreviacion'=>'Camp'],
            ['id'=>'05', 'nombre'=>'Coahuila', 'abreviacion'=>'Coah'],
            ['id'=>'06', 'nombre'=>'Colima', 'abreviacion'=>'Col'],
            ['id'=>'07', 'nombre'=>'Chiapas', 'abreviacion'=>'Chis'],
            ['id'=>'08', 'nombre'=>'Chihuahua', 'abreviacion'=>'Chih'],
            ['id'=>'09', 'nombre'=>'Distrito Federal', 'abreviacion'=>'DF'],
            ['id'=>'10', 'nombre'=>'Durango', 'abreviacion'=>'Dgo'],
            ['id'=>'11', 'nombre'=>'Guanajuato', 'abreviacion'=>'Gto'],
            ['id'=>'12', 'nombre'=>'Guerrero', 'abreviacion'=>'Gro'],
            ['id'=>'13', 'nombre'=>'Hidalgo', 'abreviacion'=>'Hgo'],
            ['id'=>'14', 'nombre'=>'Jalisco', 'abreviacion'=>'Jal'],
            ['id'=>'15', 'nombre'=>'México', 'abreviacion'=>'Mex'],
            ['id'=>'16', 'nombre'=>'Michoacán', 'abreviacion'=>'Mich'],
            ['id'=>'17', 'nombre'=>'Morelos', 'abreviacion'=>'Mor'],
            ['id'=>'18', 'nombre'=>'Nayarit', 'abreviacion'=>'Nay'],
            ['id'=>'19', 'nombre'=>'Nuevo León', 'abreviacion'=>'NL'],
            ['id'=>'20', 'nombre'=>'Oaxaca', 'abreviacion'=>'Oax'],
            ['id'=>'21', 'nombre'=>'Puebla', 'abreviacion'=>'Pue'],
            ['id'=>'22', 'nombre'=>'Querétaro', 'abreviacion'=>'Qro'],
            ['id'=>'23', 'nombre'=>'Quintana Roo', 'abreviacion'=>'Q. Roo'],
            ['id'=>'24', 'nombre'=>'San Luis Potosí', 'abreviacion'=>'SLP'],
            ['id'=>'25', 'nombre'=>'Sinaloa', 'abreviacion'=>'Sin'],
            ['id'=>'26', 'nombre'=>'Sonora', 'abreviacion'=>'Son'],
            ['id'=>'27', 'nombre'=>'Tabasco', 'abreviacion'=>'Tab'],
            ['id'=>'28', 'nombre'=>'Tamaulipas', 'abreviacion'=>'Tamps'],
            ['id'=>'29', 'nombre'=>'Tlaxcala', 'abreviacion'=>'Tlax'],
            ['id'=>'30', 'nombre'=>'Veracruz', 'abreviacion'=>'Ver'],
            ['id'=>'31', 'nombre'=>'Yucatán', 'abreviacion'=>'Yuc'],
            ['id'=>'32', 'nombre'=>'Zacatecas', 'abreviacion'=>'Zac'],
        ];
        for ($i=0; $i < count($estados); $i++) { 
            $estados[$i]['slug']=str_slug($estados[$i]['nombre'],'-');            
        }        
        Estado::insert($estados);
    }
}
