<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Eduardo Aranda',
                'email' => 'eduardoarandah@gmail.com',
                'password' => bcrypt('asdf'),
                'remember_token' => NULL,
                'created_at' => '2017-07-07 18:00:11',
                'updated_at' => '2017-07-07 18:00:11',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'usuario',
                'email' => 'usuario@prueba.com',
                'password' => bcrypt('asdf'),
                'remember_token' => NULL,
                'created_at' => '2017-07-07 18:17:11',
                'updated_at' => '2017-07-07 18:18:00',
            ),
        ));
        
        
    }
}